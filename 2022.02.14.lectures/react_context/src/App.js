import { ContextProvider, AppContext } from "./AppProvider";
import { useContext, useRef } from "react";
import './App.css';



const App = () =>
<div className="main-wrapper">
    <ContextProvider>
      <FormComponent/>
      <SavedInfoComponent/>
    </ContextProvider>
</div>
const FormComponent = () => {
  const formRef = useRef(null);
  const [state, setState ] = useContext (AppContext ); // Get state prop from context
  const langId = state.setLang; 

  const savePersonalInfo = () => {
    console.log("at savePersonalinfo");
    const formInputs = formRef.current.elements;
    console.log(formInputs.name.value);

    setState( (state) => {
      return {...state,savedPersonalInformation : 
        {"name":formInputs.name.value,"nationality":formInputs.nationality.value,"address":formInputs.address.value,
        "major":formInputs.major.value}}
    });
  }

  return (
  <div>
      <h1>{state.title[langId]}</h1>
      <form ref={formRef}>
        <label htmlFor="name" className="formElement"> {state.name[langId]}: </label>
        <input type="text" className="inputElement" id="name" name="name" /><br/>
        <label htmlFor="nationality" className="formElement"> {state.nationality[langId]}: </label>
        <input type="text" className="inputElement" id="nationality" name="nationality" /><br/>
        <label htmlFor="address" className="formElement"> {state.address[langId]}: </label>
        <input type="text" className="inputElement" id="address" name="address" /><br/>
        <label htmlFor="major" className="formElement"> {state.major[langId]}: </label>
        <input type="text" className="inputElement" id="major" name="major" /><br/>
      </form>
      <button type="button" className="savebutton" onClick={savePersonalInfo}>{state.save[langId]}</button> 
  </div>
  )
};

const SavedInfoComponent = () => {
  const [state, setState ] = useContext (AppContext ); // Get state prop from context
  const langId = state.setLang; 

  const switchLang = () => {
    const newLang = (state.setLang === 0 ? 1 : 0);
    // must return the state
    setState( (state) => {
      return {...state, setLang : newLang}
    });
  }
  
  return (
  <div>
    <div className="savedInfo">
      <p>
       <b> {state.name[langId]}</b> : {state.savedPersonalInformation["name"]} <br/>
       <b> {state.nationality[langId]}</b> : {state.savedPersonalInformation["nationality"]} <br/>
       <b> {state.address[langId]}</b> : {state.savedPersonalInformation["address"]} <br/>
       <b> {state.major[langId]}</b> : {state.savedPersonalInformation["major"]} <br/>
      </p>
    </div>
  <button type="button" className="switchlangbutton" onClick={switchLang}>{state.switch[langId]}</button> 
  </div>
  )
};

export default App;
