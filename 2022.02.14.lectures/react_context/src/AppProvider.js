import { createContext, useState } from "react";

export const AppContext = createContext ([{}, () => {}]); // Create context

export const ContextProvider = (props) => {
    const initialContext = {
        languages: ["fi","en"],
        setLang: 0,
        title: ["Opiskelijan tiedot","Student information"],
        name: ["Nimi","Name"],
        nationality: ["Kansallisuus","Nationality"],
        address: ["Osoite","Address"],
        major: ["Pääaine","Major subject"],
        save: ["Tallenna","Save"],
        switch: ["Vaihda kieltä","Switch language"],
        saveButtonPressed : false,
        savedPersonalInformation : {"name":"","nationality":"","address":"","major":""},
    }

const [state, setState ] = useState (initialContext ); // Props we want child components to have

return (
    <AppContext.Provider value={[state, setState ]}> 
        {props.children } 
    </AppContext.Provider >
    );
}