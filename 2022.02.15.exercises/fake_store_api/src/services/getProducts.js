import axios from "axios";

const baseURL = "https://fakestoreapi.com/products/";

export const getAllProducts = async () => {

    const products = await axios.get(`${baseURL}`);
    // console.log("in getAllProducts()");
    // console.log(products.data);
    return products.data;
};

export const getAllCategories = async () => {

    const categories = await axios.get(`${baseURL}/categories`);
    // console.log("in getAllProducts()");
    // console.log(products.data);
    return categories.data;
};